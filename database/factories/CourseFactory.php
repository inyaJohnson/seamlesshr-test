<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        //
        'text' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'title' => $faker->text($maxNbChars = 50),
        'code' => $faker->unique()->numerify('ECE ###'),
        'lecturer' => $faker->name
    ];
});
