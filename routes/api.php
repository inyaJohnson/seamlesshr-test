<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/register', 'AuthController@register');


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'UserController@profile')->middleware('auth:api');

});

Route::group([
    'prefix' => 'course'

], function ($router) {

    Route::post('create', 'CourseController@store')->name('course.create');
    Route::post('register', 'UserController@register')->name('course.register')->middleware('auth:api');
    Route::get('export', 'CourseController@export')->name('course.export');

});


Route::get('error', 'AuthController@error')->name('error');
