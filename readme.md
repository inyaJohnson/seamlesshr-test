## About Test
Clone test repo @ <a href=" https://bitbucket.org/insidify/seamlesshr-test/src/master/"> Seamless Test </a>

## Task 
Create Endpoints for Login, Registration and creation of users and courses

## Tools
- IDE (PHPSTORM) preferably
- HTTP client (Postman) preferably

## Step
- Run `git clone https://inyaJohnson@bitbucket.org/inyaJohnson/seamlesshr-test.git`
- Create a .env file in the parent folder and copy the content of .env.example into the .env file
- Run `php artisan key:generate`
- Run `composer install` to install the dependencies
- Run `php artisan serve` to start your application
- Use postman or any other web client  of your choice to access the endpoints


## Endpoints Created
Implement the jwt for the registration and creation of user, the jwt is a web token that is used to implement 
authentication and authorization.

- POST => http://127.0.0.1:8000/api/register

- POST => http://127.0.0.1:8000/api/auth/login

- POST => http://127.0.0.1:8000/api/course/create

- GET => http://127.0.0.1:8000/api/auth/user

- POST => http://127.0.0.1:8000/api/course/register

- GET => http://127.0.0.1:8000/api/course/export

## Sample of Returns json data from one of the APIs.

### URL

/api/auth/user

### Method:

GET

### URL Params

Required: Auth 

### Success Response:

Code: 200

{
        
		"id": 1,
        "text": "Expedita provident sit a optio voluptas recusandae.",
        "created_at": "2020-02-08 12:02:06",
        "updated_at": "2020-02-08 12:02:06",
        "title": "Nostrum odit quis non voluptatum non.",
        "code": "ECE 282",
        "lecturer": "Addie Yost",
        "pivot": {
            "user_id": 1,
            "course_id": 1,
            "created_at": "2020-02-08 12:35:19",
            "updated_at": "2020-02-08 12:35:19"
        }
    }
	
### Error Response:

Code: 404 NOT FOUND
Content: { error : "User doesn't exist" }

OR

Code: 401 UNAUTHORIZED
Content: { error : "You are unauthorized to make this request." }

 
## Author
Inya Johnson

## Task By 
SeamlessHR