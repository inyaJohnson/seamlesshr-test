<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserCourse extends Pivot
{
    public $incrementing = true;

    public $table = "user_course";

    protected $fillable = [
        'user_id',
        'course_id',
    ];

}