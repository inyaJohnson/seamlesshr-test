<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\RegisterCourseRequest;
use App\UserCourse;


class UserController extends Controller
{
    public function register(RegisterCourseRequest $request){
        // return $request->code;
        $course = Course::where('code', $request->code)->firstOrFail();

        $userCourse = UserCourse::create([
            'course_id' => $course->id,
            'user_id' => auth()->user()->id
        ]);

        $message = "User could not be registered for course";

        if ($userCourse) {
            $message = "Course successfully registered";
        }

        return response()->json(['message' => $message]);
    }


    public function profile(){
        $user = auth()->user();
        return $user->courses;
    }

}
