<?php

namespace App\Http\Controllers;

use App\Jobs\CreateCourseJob;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{

    public function store()
    {
        //
        CreateCourseJob::dispatch()
            ->delay(now()->addSeconds(10));

        return response('Courses created successfully', 200)
            ->header('Content-Type', 'text/plain');
    }


    public function export(){
        return Excel::download(new CoursesExport, 'courses.xlsx');

    }

}
